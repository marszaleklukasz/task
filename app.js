var express = require('express');
var session = require('express-session');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
  
var mongoose = require('mongoose');
var models = require('./models');
var dbUrl = process.env.MONGOHQ_URL || 'mongodb://@localhost:27017/task';
var db = mongoose.connect(dbUrl, { safe: true });
  
var users = require('./routes/users');

var app = express();

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session({
   secret: '54548579-HFJHSDJK',
   resave: true,
   saveUninitialized: true
}));

app.use(function(req, res, next) {
  req.models = models;
  return next();
});

app.use('/v1/users', users);

app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.json({ error: err.message });
});

module.exports = app;
