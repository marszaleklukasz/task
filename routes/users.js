var express = require('express');
var router = express.Router();
var onlyLogged = require('../lib/onlyLogged');
var onlyUnlogged = require('../lib/onlyUnlogged');
var hasRoles = require('../lib/hasRoles');

// /v1/users/login
router.post('/login', onlyUnlogged, function(req, res, next) {
    if (!req.body.name || !req.body.password) {
        var err = new Error('Bad Request');
        err.status = 400;
        next(err);
    }
  
    req.models.User.findOne({
        name: req.body.name,
        password: req.body.password
    }, function(error, user) {
        if (error) return next(error);
        
        if (!user) {
            var err = new Error('Unauthorized');
            err.status = 401;
            return next(err);
        }
        
        req.session.user = user;
        req.session.isLogged = true;
        res.json();
    });
});

// /v1/users/logout
router.post('/logout', onlyLogged, function(req, res, next) {
    req.session.destroy();
    res.json();
});

// /v1/users/register
router.post('/register', onlyUnlogged, function(req, res, next) {
    if (!req.body.name && !req.body.password && !req.body.role) {
        var err = new Error('Bad Request');
        err.status = 400;
        next(err);
    }
    
    var user = {
        name: req.body.name,
        password: req.body.password,
        role: req.body.role
    };
    
    req.models.User.create(user, function(error, userResponse) {
        if (error) return next(error);
        res.json();
    });
});

// /v1/users/:id
router.get('/:id', onlyLogged, function(req, res, next) {
    if (!req.params.id) {
        var err = new Error('Bad Request');
        err.status = 400;
        return next(err);
    }
    
    if (!hasRoles(req, ['editor', 'admin'])) {
        var err = new Error('Unauthorized');
        err.status = 401;
        return next(err);
    }    
    
    req.models.User.findById(req.params.id, function(error, user) {
        if (error) return next(error);
        
        if (!user) {
            var err = new Error('Bad Request');
            err.status = 400;
            return next(err);
        }
        
        res.json(user);
    });
});

// /v1/users/:id
router.post('/:id', onlyLogged, function(req, res, next) {
    if (!req.params.id) {
        var err = new Error('Bad Request');
        err.status = 400;
        return next(err);
    }
    
    if (!hasRoles(req, ['editor', 'admin']) && req.params.id != req.session.user._id) {
        var err = new Error('Unauthorized');
        err.status = 401;
        return next(err);
    }    
  
    req.models.User.findById(req.params.id, function(error, user) {
        if (error) return next(error);
        
        if (!user) {
            var err = new Error('Bad Request');
            err.status = 400;
            return next(err);
        }        

        switch (req.session.user.role) {
            case 'editor':
                if (user.role == 'admin' || req.body.role == 'admin' ) {
                    var err = new Error('Bad Request');
                    err.status = 400;
                    return next(err);
                }
                
                break;
            case 'user':
                if (req.body.role != user.role) {
                    var err = new Error('Bad Request');
                    err.status = 400;
                    return next(err);
                }
                
                break;
        }

        var userData = {
            name: req.body.name,
            password: req.body.password,
            role: req.body.role
        };

        user.update({ $set: userData }, function(error, count, raw) {
            if (error) return next(error);
            res.json();
        });
    });
});

// /v1/users/:id
router.delete('/:id', onlyLogged, function(req, res, next) {
    if (!req.params.id) {
        var err = new Error('Bad Request');
        err.status = 400;
        return next(err);
    }
    
    if (!hasRoles(req, ['editor', 'admin'])) {
        var err = new Error('Unauthorized');
        err.status = 401;
        return next(err);
    }
    
    req.models.User.findById(req.params.id, function(error, user) {
        if (error) return next(error);
        
        if (!user) {
            var err = new Error('Bad Request');
            err.status = 400;
            return next(err);
        }
        
        user.remove(function(error, doc){
            if (error) return next(error);
            res.json();
        });
    });
});

module.exports = router;
