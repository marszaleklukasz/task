var onlyLogged = function(req, res, next) {
    if (req.session && req.session.isLogged) {
        return next();
    } else {
        var err = new Error('Unauthorized');
        err.status = 401;
        next(err);
    }
};

module.exports = onlyLogged;