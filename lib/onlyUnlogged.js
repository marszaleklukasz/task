var onlyUnlogged = function(req, res, next) {
    if (!req.session.isLogged) {
        return next();
    } else {
        var err = new Error('Unauthorized');
        err.status = 401;
        next(err);
    }
};

module.exports = onlyUnlogged;